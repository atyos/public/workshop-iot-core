# Tutorial workshop AWS IoT Core — MQTT avec Raspberry Pi Pico

## Introduction

### Protocole MQTT

Le protocole MQTT est un protocole de messagerie léger utilisé pour la communication de machine à machine sur un réseau à bande passante restreinte et à ressources minimales. Il se compose de trois éléments principaux : un client MQTT, un
broker MQTT et une connexion MQTT.

* **Client MQTT** : Une machine qui envoie ou reçoit des messages MQTT. Un appareil est appelé "publisher" s'il envoie des données, et "subscriber" s'il reçoit des données. Un appareil peut également remplir les deux rôles simultanément.
* **Broker MQTT** : Un serveur qui coordonne les messages MQTT entre les différents clients.

La connexion entre le client et le broker commence par un message `CONNECT` envoyé par le client. Le broker confirme
ensuite l'établissement de la connexion en envoyant un message `CONNACK` sur la pile TCP/IP.

Une fois connecté, le client peut publier (**publish**) des messages sur un sujet (**topic**) auquel d'autres clients peuvent s'abonner (**subscribe**) pour recevoir les messages.

![](tutorial/assets/00_diagram.png)

#### Liste des topics

##### Vers lesquels l'appareil publie des données

|                                              | Topic                            |
| -------------------------------------------- | -------------------------------- |
| Réponse "Ping"                               | pico/+/data/ping                 |
| Réponse de requête de lecture de température | pico/+/data/am2320/temperature   |
| Réponse de requête de lecture d'humidité     | pico/+/data/am2320/humidity      |
| Réponse de requête de lecture de statut LED  | pico/+/data/led/status/read      |
| Réponse de requête d'écriture de statut LED  | pico/+/led/data/led/status/write |

##### Depuis lesquel l'appareil écoute

|                                   | Topic                     |
| --------------------------------- | ------------------------- |
| Requête Ping                      | pico/+/ping               |
| Requête de lecture de température | pico/+/am2320/temperature |
| Requête de lecture d'humidité     | pico/+/am2320/humidity    |
| Requête de lecture de statut LED  | pico/+/led/status/read    |
| Requête de lecture de statut LED  | pico/+/led/status/write   |

### Déroulement du workshop

Ce tutoriel a pour objectif de vous guider à travers vos premiers pas sur la plateforme AWS IoT, en créant un appareil et en envoyant/recevant des données via AWS IoT Core.

À tout moment, vous pourrez interpeller un des intervenants d'Atyos pour vous aider dans la progression du tutoriel.

### Matériel nécessaire

- 1x Raspberry Pi Pico W
- 1x Câble d'alimentation USB
- 1x LED RGB
- 1x Capteur de température et d'humidité AM2320

![](tutorial/assets/circuit.png)

### Pré-requis

### Python

Assurez-vous d'avoir Python 3.11.x. Il peut être installé :

- Windows via [Python Release Python 3.11.9 | Python.org](https://www.python.org/downloads/release/python-3119/) (assurez-vous d'avoir coché la case "Ajouter au PATH")

- MacOS (homebrew) via `brew install python@3.11`

- Linux via votre gestionnaire de paquet préféré

Puis activez votre environnement avec :

```shell
source .venv/bin/activate
```

ou sur Windows (Powershell) :

```powershell
.venv\Scripts\Activate.ps1
```

### Installer Ampy

Ampy est une librairie d'Adafruit pour interagir avec un appareil utilisant CircuitPython ou MicroPython via une connection série.

Assurez-vous d'être dans le dossier où l'environnement virtuel est activé, puis installez ampy avec :

```shell
pip install adafruit-ampy
```

### Installer Picocom/PuTTY

Sur MacOS (homebrew), vous pouvez installer Picocom a vec la commande :

```shell
brew install picocom
```

Sur Linux (Ubuntu), avec :

```shell
sudo apt-get install picocom
```

Sur Windows, avec PuTTY :

- Lien de [téléchargement](https://www.putty.org/)

- Tutorial pour utiliser [PuTTY avec le port COM]([Using PuTTY for Serial](https://pbxbook.com/voip/sputty.html)
  
  - Baud rate : 115200
  
  - Vous pouvez laisser le reste tel quel

## Étape 0 : Activez votre compte AWS Workshop

[Lien pour la création de compte](https://catalog.us-east-1.prod.workshops.aws/join?access-code=8c54-08f1f2-c7 "https://catalog.us-east-1.prod.workshops.aws/join?access-code=8c54-08f1f2-c7"), munissez vous de votre adresse e-mail.

Cet utilitaire vous permettra de créer un compte disponible pendant 48 heures grâce à un code d'accès.

Dans la fenêtre qui s'affiche, choisissez "**Email one-time password (OTP)**", puis écrivez votre adresse e-mail. Ensuite, "**Envoyer le code d'accès**"

Récupérez le code d'accès dans votre boîte mail, puis validez la création du compte.

Enfin, acceptez les termes et conditions de l'utilisation du service en cochant "**I agree with the Terms and Conditions**".

## Étape 1 : Créez votre premier appareil

Dans la console AWS, saisissez "AWS IoT Core" dans la barre de recherche, puis appuyez sur "Entrée" pour accéder au tableau de bord AWS IoT.

![](tutorial/assets/0101.png)

Dans le menu "Gérer" à gauche, sélectionnez "Tous les appareils", puis "**Objets**". Une page s'affichera avec tous les objets associés à votre compte. Cliquez sur "**Créer des objets**".

![](tutorial/assets/0102.png)

Sur la page suivante, choisissez "**Créer un objet**" pour créer une représentation de l'appareil sur la plateforme AWS IoT. Cliquez sur "**Suivant**".

![](tutorial/assets/0103.png)

Dans les propriétés de l'objet, entrez un nom qui permet d'identifier facilement votre appareil : `workshop-raspberrypi-pico-X`. Remplacez `X` par le numéro associé à votre appareil.

![](tutorial/assets/0104.png)

Vous pouvez laisser les autres champs par défaut. Continuez en cliquant sur "**Suivant**".

## Étape 2 : Mettre en place les stratégies du certificat

Un certificat est requis pour se connecter à AWS IoT. Il est signé par une autorité de certification (CA) afin d'identifier la provenance de la requête du client. Il est possible de laisser AWS générer un certificat avec le CA d'Amazon Root, mais aussi ajouter votre propre certificat.

Dans notre exemple, nous allons utiliser un certificat auto-généré par IoT Core. Choisissez "**Génération automatique d'un nouveau certificat**", puis cliquez sur "**Suivant**".

![](tutorial/assets/0201.png)

Ensuite, nous allons attacher des stratégies au certificat pour limiter l'accès aux ressources AWS IoT. Cliquez sur "**Créer une stratégie**" pour ouvrir une nouvelle fenêtre.

![](tutorial/assets/0202.png)

Dans le champ "**Nom de la politique**", saisissez `workshop-raspberrypi-pico-policy-X`. Dans la section "**Document de politique**", choisissez "**JSON**".

![](tutorial/assets/0203.png)

Copiez-collez le contenu du fichier suivant dans le champ prévu pour le document de politique :

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Condition": {
        "Bool": {
          "iot:Connection.Thing.IsAttached": "true"
        }
      },
      "Effect": "Allow",
      "Action": "iot:Connect",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iot:Publish"
      ],
      "Resource": [
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/data/ping",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/data/**/read",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/data/**/write"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "iot:Receive"
      ],
      "Resource": [
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/ping",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/**/read",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/**/write"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "iot:Subscribe",
      "Resource": [
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/ping",
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/**/write",
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/**/read"
      ]
    }
  ]
}
```

Puis cliquez sur "**Créer**".

![](tutorial/assets/0204.png)

En cliquant sur le nom de la politique nouvellement créée, vous pourrez vérifier la bonne prise en compte des politiques définies dans le JSON, comme illustré ci-dessous :

![](tutorial/assets/0205.png)

Cette politique autorise l'appareil à publier (publish) et s'abonner (subscribe) à des topics spécifiques. L'appareil ne pourra donc pas envoyer ou recevoir des messages en dehors du pattern spécifié par l'Action de politique et la Ressource de politique.

## Étape 3 : Télécharger les fichiers générés

Retournez sur la fenêtre des **Stratégies**. Cochez la case à gauche du nom de la politique créée à l'étape 2, puis cliquez sur "**Créer un objet**" pour finir cet utilitaire de création d'objet.

Une fenêtre s'affiche pour télécharger les données suivantes :

- Certificat
- Clé publique
- Clé privée
- Certificats CA (RSA 2048 bit CA1, ECC 256 bit CA3)

![](tutorial/assets/0301.png)

Téléchargez les fichiers suivants :

- Le certificat d'appareil

- Le fichier de clé publique

- Le fichier de clé privée

Puis fermez la fenêtre en cliquant sur "**Terminé**".

## Étape 4 : Tester l'appareil

### Renommer fichiers de certificat

Dans le menu "Tester", cliquez sur "Client de test MQTT". Une fenêtre s'affiche avec les détails du client de test MQTT ; notez la valeur du point de terminaison dans "Détails de connexion".

![](tutorial/assets/0401.png)

Dans le dossier du dépôt de votre machine, déplacez le certificat d'appareil et le fichier de clé privée dans le dossier `./microcontroller/`. Renommez les fichiers comme suit :

- Le certificat d'appareil : `cert.pem.crt`
- La clé privée : `private.pem.key`

![](tutorial/assets/0402.png)

### Modifier des variables dans le code

Le fichier contenant le script exécuté dans l'appareil se nomme `main.py`. Il contient le code nécessaire pour contrôler la LED RGB et pour interroger les données du capteur. Certaines variables y sont déjà pré-remplies, mais d'autresnécessitent votre intervention. 

Dans le fichier `main.py`, modifiez les lignes suivantes :

```python
def init(board):
    ...
    setup = PicoWSetup(
        thing_name=b"votre_nom_d_appareil", # défini dans l'utilitaire de création d'objet
        aws_mqtt_endpoint=b"votre_endpoint.iot.us-east-1.amazonaws.com", # le point de terminaison client MQTT
        private_key="private.pem.key", # la clé privée
        private_cert="cert.pem.crt" # le certificat
    )
    ...
```

Assurez-vous que les valeurs sont correctes, puis enregistrez le fichier.

### Flasher l'appareil

Pour les utilisateurs de Linux et MacOS script de flash a été mis à votre disposition dans `microcontroller/flash.sh`.

Le script va copier les fichiers de certificats et les fichiers sources, puis lancer `picocom` afin de lire la sortie de l'appareil.

#### Note : Pour les utilisateurs de MacOS

Vérifiez le port tty utilisé par votre appareil, avec la commande suivante :

```shell
ls /dev/tty.*
```

![](tutorial/assets/0403.png)

Si la valeur affichée est différente de `/dev/tty.usbmodem101`, veuillez remplacer la ligne 4 du script par la valeur de votre port tty.

```shell
    Darwin*)    DEVICE_PATH="/dev/tty.usbmodem101";; # ICI
```

### Exécuter le script

Depuis la racine du projet, exécutez cette commande pour accorder au script les permissions d'exécution :

```shell
chmod +x microcontroller/flash.sh
```

Puis, exécutez les commandes :

```shell
cd ./microcontroller/
./flash.sh
```

Et, lorsque picocom sera lancé, appuyez sur les boutons `Ctrl + D` (ou `control + D` sur MacOS) pour mettre en marche l'appareil. Si tout fonctionne, la LED devrait être allumée en vert.

Note : Pour sortir de picocom, appuyez sur les boutons `Ctrl + A` puis tout en gardant `Ctrl` d'appuyé, `X`.

## Étape 5 : Envoyer des données MQTT via le client MQTT

Dans le client MQTT d'AWS IoT, abonnez-vous aux topics spécifiés dans l'introduction (ceux vers lesquels l'appareil publie) dans l'onglet "**S'abonner à une rubrique**".

![](tutorial/assets/0501.png)

Le client vous permettra de publier vers les topics auxquels l'appareil écoute, puis écouter les topics vers lesquels l'appareil publie. 

Dans l'onglet "**Publier dans une rubrique**", essayez d'envoyer un message vers le topic

```
pico/<thing_name>/ping
```

Écrivez un message JSON contenant au moins un champ nommé "event_id":

```json
{
    "event_id": "bonjour"
}
```

![](tutorial/assets/0502.png) 

Puis cliquez sur "**Publier**".

En bas de la page dans Abonnements, si tout se passe bien, vous recevez la réponse dans le topic de la réponse, avec la valeur de "**event_id**" que vous aviez envoyé :

![](tutorial/assets/0503.png) 

## Étape 6 : Envoyer des données MQTT via le client Streamlit

On aimerait publish des données depuis le client Streamlit vers les topics sur lesquels le Raspberry Pi Pico écoute :

- pico/<thing_name>/ping

- pico/<thing_name>/**/read

- pico/<thing_name>/**/write

Pour cela, il faut créer un appareil sur AWS IoT et lui assigner un certificat. Répétez l'étape 1 à 3 pour créer l'appareil et télécharger son certificat et sa clé privée.

Note : nommez l'appareil `workshop-streamlit-client-X`, et la policy `workshop-streamlit-client-policy-X`.

La policy à copier/coller est la suivante :

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Condition": {
        "Bool": {
          "iot:Connection.Thing.IsAttached": "true"
        }
      },
      "Effect": "Allow",
      "Action": "iot:Connect",
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "iot:Subscribe"
      ],
      "Resource": [
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/data/ping",
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/data/**/read",
        "arn:aws:iot:us-east-1:*:topicfilter/pico/${iot:Connection.Thing.ThingName}/data/**/write"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "iot:Receive"
      ],
      "Resource": [
        "arn:aws:iot:us-east-1:*:topic/pico/data/${iot:Connection.Thing.ThingName}/ping",
        "arn:aws:iot:us-east-1:*:topic/pico/data/${iot:Connection.Thing.ThingName}/**/read",
        "arn:aws:iot:us-east-1:*:topic/pico/data/${iot:Connection.Thing.ThingName}/**/write"
      ]
    },
    {
      "Effect": "Allow",
      "Action": "iot:Publish",
      "Resource": [
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/ping",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/**/write",
        "arn:aws:iot:us-east-1:*:topic/pico/${iot:Connection.Thing.ThingName}/**/read"
      ]
    }
  ]
}
```
