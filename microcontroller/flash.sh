unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     DEVICE_PATH="/dev/ttyACM0";;
    Darwin*)    DEVICE_PATH="/dev/tty.usbmodem101";;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    MSYS_NT*)   machine=Git;;
    *)          machine="UNKNOWN:${unameOut}"
esac

for filename in src/*.py; do
  echo "put ${filename}"
  ampy -p "${DEVICE_PATH}" put "${filename}" "$(basename "${filename}")" || exit 1
done

echo "put source files ok"

sleep 0.5s

for filename in *.pem.*; do
  echo "put ${filename}"
  ampy -p "${DEVICE_PATH}" put "${filename}" "$(basename "${filename}")" || exit 1
done

#ampy -p "${DEVICE_PATH}" reset --repl || exit 1
#
#sleep 0.5s

screen -r -S PicomSess -X quit > /dev/null
screen -e^Tt -d -m -S PicomSess picocom "${DEVICE_PATH}" -b115200

sleep 0.5 && screen -S PicomSess -p 0 -X stuff "^D" &

screen -r PicomSess
