from machine import PWM
from time import sleep_ms

from mcauser_am2320 import AM2320
from setup import PicoWSetup


class Board:
    RGB_LED_FREQ: int = 1000
    RGB_LED_PIN_RED: PWM = PWM(PicoWSetup.LED_RED_PIN, freq=RGB_LED_FREQ)
    RGB_LED_PIN_GREEN: PWM = PWM(
        PicoWSetup.LED_GREEN_PIN, freq=RGB_LED_FREQ
    )
    RGB_LED_PIN_BLUE: PWM = PWM(
        PicoWSetup.LED_BLUE_PIN, freq=RGB_LED_FREQ
    )
    LAST_COLOR: tuple[int, int, int] = None
    ACM2320_SENSOR = AM2320()

    TEMPS = []
    HUMIDITY = []

    @staticmethod
    def map_color_range(x, in_min, in_max, out_min, out_max):
        return (x - in_min) * (out_max - out_min) // (in_max - in_min) + out_min

    @classmethod
    def set_led_color(cls, color: tuple[int, int, int]):
        red, green, blue = color
        red = int(red)
        green = int(green)
        blue = int(blue)
        cls.RGB_LED_PIN_RED.duty_u16(cls.map_color_range(red, 0, 255, 0, 65535))
        cls.RGB_LED_PIN_GREEN.duty_u16(cls.map_color_range(green, 0, 255, 0, 65535))
        cls.RGB_LED_PIN_BLUE.duty_u16(cls.map_color_range(blue, 0, 255, 0, 65535))

    @classmethod
    def get_led_color(cls):
        return (
            cls.map_color_range(cls.RGB_LED_PIN_RED.duty_u16(), 0, 65535, 0, 255),
            cls.map_color_range(cls.RGB_LED_PIN_GREEN.duty_u16(), 0, 65535, 0, 255),
            cls.map_color_range(cls.RGB_LED_PIN_BLUE.duty_u16(), 0, 65535, 0, 255),
        )

    @classmethod
    def get_led_state(cls):
        current_led_color = cls.get_led_color()
        if current_led_color == (0, 0, 0):
            return "off"
        else:
            return "on"

    @classmethod
    def set_led_state(cls, state: str):
        current_led_color = cls.get_led_color()
        if state == "on" and current_led_color == (0, 0, 0):
            cls.set_led_color(cls.LAST_COLOR or (255, 255, 255))
        else:
            cls.LAST_COLOR = current_led_color
            cls.set_led_color((0, 0, 0))

    @classmethod
    def store_measurements(cls, temp, humidity):
        cls.TEMPS.append(temp)
        cls.HUMIDITY.append(humidity)

        if len(cls.TEMPS) > 10:
            cls.TEMPS.pop(0)

        if len(cls.HUMIDITY) > 10:
            cls.HUMIDITY.pop(0)

    @classmethod
    def measure(cls) -> (float, float):
        cls.ACM2320_SENSOR.measure()
        temp = cls.ACM2320_SENSOR.temperature()
        humidity = cls.ACM2320_SENSOR.humidity()

        cls.store_measurements(temp, humidity)

        return temp, humidity

    @classmethod
    def get_avg_temp(cls):
        return sum(list(cls.TEMPS)) / len(list(cls.TEMPS))

    @classmethod
    def get_avg_humidity(cls):
        return sum(list(cls.HUMIDITY)) / len(list(cls.HUMIDITY))

    @classmethod
    def check_sensor(cls):
        num_check = 5
        errors = []
        for _ in range(num_check):
            try:
                cls.ACM2320_SENSOR.check()
                sleep_ms(100)
            except Exception as e:
                errors.append(e)

        if len(errors) == num_check:
            raise OSError(f"Can't use AM2320 sensor: {errors[-1]}")

        print("Sensor OK")
