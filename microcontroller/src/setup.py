import mip
import ubinascii
from micropython import const

from mqtt import init_topics, mqtt_callback


class PicoWSetup:
    WIFI_SSID: str = "TODO"
    WIFI_PASSWORD: str = "TODO"
    I2C_ID_SCL_SDA: tuple[int, int, int] = (1, 15, 14)  # ID, SCL, SDA
    I2C_FREQ: int = const(400_00)
    LED_RED_PIN: int = 16
    LED_GREEN_PIN: int = 17
    LED_BLUE_PIN: int = 18
    LIBS = {"umqtt.simple": "1.3.4"}

    client_id: bytes
    thing_name: bytes
    aws_mqtt_endpoint: bytes
    private_key: str
    private_cert: str
    device_ip: str = None

    def __init__(self, thing_name: bytes, aws_mqtt_endpoint: bytes, private_key: str,
                 private_cert: str):
        self.thing_name = thing_name
        self.client_id = thing_name
        self.aws_mqtt_endpoint = aws_mqtt_endpoint
        self.private_key = private_key
        self.private_cert = private_cert

    def connect_wifi(self):
        import network
        network.hostname(self.thing_name)

        wlan = network.WLAN(network.STA_IF)

        wlan.active(True)

        if not wlan.isconnected():
            print('Connecting to network...')
            wlan.connect(self.WIFI_SSID, self.WIFI_PASSWORD)
            while not wlan.isconnected():
                ...

        self.device_ip, *_ = wlan.ifconfig()

        print('Network config:', wlan.ifconfig())

    def install_libs(self):
        for lib, version in self.LIBS.items():
            optional_params = {}
            if version:
                optional_params = {"version": version}
            mip.install(lib, **optional_params)

    @staticmethod
    def _format_pem(pem_content):
        pem_content = pem_content.strip()
        pem_content = pem_content.split("\n")
        pem_content = "".join(pem_content[1:-1])
        return ubinascii.a2b_base64(pem_content)

    def get_mqtt_client(self, board):
        from umqtt.simple import MQTTClient

        with open(self.private_cert, "r") as f:
            cert = self._format_pem(f.read())
        with open(self.private_key, "r") as f:
            key = self._format_pem(f.read())

        ssl_params = {"key": key, "cert": cert, "server_side": False}

        mqtt = MQTTClient(client_id=self.client_id, server=self.aws_mqtt_endpoint,
                          port=8883, keepalive=1200,
                          ssl=True, ssl_params=ssl_params)

        print("Connecting to AWS IoT...")
        mqtt.connect()
        print("Connect done")

        mqtt.set_callback(lambda topic, msg: None)

        print("Init topics...")
        topics = init_topics(
            topics_prefix=b"pico",
            mqtt_client=mqtt,
            board_setup=self,
            board=board,
        )

        mqtt.set_callback(lambda topic, msg: mqtt_callback(topics, mqtt, topic, msg))

        print("Init topics done")

        return mqtt
