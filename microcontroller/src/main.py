from time import sleep, sleep_ms

from board import Board
from setup import PicoWSetup

MEASUREMENTS_PER_SEC = 10


def init(board):
    board.set_led_color((255, 80, 0))

    setup = PicoWSetup(
        thing_name=b"thing_name",
        aws_mqtt_endpoint=b"endpoint.iot.eu-west-1.amazonaws.com",
        private_key="private.pem.key",
        private_cert="cert.pem.crt"
    )
    setup.connect_wifi()
    setup.install_libs()

    board.check_sensor()

    mqtt_client = setup.get_mqtt_client(board)

    board.set_led_color((0, 255, 0))

    return mqtt_client


def main_loop(board, mqtt_client):
    while True:
        try:
            board.measure()
            # print(f"temp: {board.get_avg_temp()} | humidity: {board.get_avg_humidity()}")
            mqtt_client.check_msg()
        except OSError as e:
            print(e)
        sleep(1)


def main():
    board = Board()

    mqtt_client = None

    try:
        mqtt_client = init(board)

        print("Hello ! finished init")

        main_loop(board, mqtt_client)
    except Exception as e:
        board.set_led_color((255, 0, 0))
        raise e
    finally:
        if mqtt_client:
            mqtt_client.disconnect()
        print("Goodbye !")


main()
