import ujson


class Topic:
    handler: "Callable[[MQTTClient, Topic, str], None]"
    subscribe_to_topic: bool
    subscribe_prefix: bytes
    publish_prefix: bytes
    topic_name: bytes
    subscribe_topic_name: bytes
    publish_topic_name: bytes

    def __init__(self, topics_prefix: bytes, thing_name: bytes,
                 *,
                 topic_name: bytes,
                 handler: "Callable[[MQTTClient, Topic, str], None]"):
        self.topic_name = topic_name
        self.handler = handler

        partial_topic_name = topics_prefix + b"/" + thing_name + b"/"

        self.full_topic_name = partial_topic_name + topic_name

        self.response_topic_name = partial_topic_name + b"data/" + topic_name

    def send_resp(self, mqtt_client, event_id, message):
        if event_id:
            message["event_id"] = event_id
        else:
            message["event_id"] = "event_id_not_found"

        mqtt_client.publish(
            self.response_topic_name,
            ujson.dumps(message).encode("utf-8")
        )


def set_led_handler(board, client, topic, msg):
    event_id = msg.get("event_id")

    if "state" not in msg and "color" not in msg:
        topic.send_resp(client, event_id, {
            "message": "invalid event, missing key 'state' "
                       "and key 'color', at least one is needed"
        })
        return

    if "state" in msg:
        if msg["state"] not in ["on", "off"]:
            topic.send_resp(client, event_id, {
                "message": "invalid event, key 'state' accept values: 'on' | 'off'"
            })
            return

        board.set_led_state(msg["state"])

    if "color" in msg:
        if not isinstance(msg["color"], list):
            topic.send_resp(client, event_id, {
                "message": "invalid event, key 'color' "
                           "needs to be a list of 3 elements [r, g, b]"
            })
            return

        if len(msg["color"]) != 3:
            topic.send_resp(client, event_id, {
                "message": "invalid event, key 'color' "
                           "needs to be a list of 3 elements [r, g, b]"
            })
            return

        board.set_led_color(msg["color"])

    topic.send_resp(client, event_id, {
        "message": "success"
    })


def init_topics(topics_prefix: bytes, board_setup, board, mqtt_client):
    ping = Topic(
        topics_prefix,
        board_setup.thing_name,
        topic_name=b"ping",
        handler=lambda client, topic, msg: topic.send_resp(
            client, msg.get("event_id"),
            {
                "message": f"Hello from {board_setup.thing_name.decode('utf-8')} "
                           + f"at ip: {board_setup.device_ip}"
            }
        )
    )

    get_led_state = Topic(
        topics_prefix,
        board_setup.thing_name,
        topic_name=b"led/status/read",
        handler=lambda client, topic, msg: topic.send_resp(
            client, msg.get("event_id"),
            {
                "state": board.get_led_state(),
                "color": board.get_led_color()
            }
        )
    )

    set_led_state = Topic(
        topics_prefix,
        board_setup.thing_name,
        topic_name=b"led/status/write",
        handler=lambda client, topic, msg: set_led_handler(board, client, topic, msg)
    )

    get_temp = Topic(
        topics_prefix,
        board_setup.thing_name,
        topic_name=b"am2320/temperature/read",
        handler=lambda client, topic, msg: topic.send_resp(
            client, msg.get("event_id"),
            {
                "temp": board.get_avg_temp(),
            }
        )
    )

    get_humidity = Topic(
        topics_prefix,
        board_setup.thing_name,
        topic_name=b"am2320/humidity/read",
        handler=lambda client, topic, msg: topic.send_resp(
            client, msg.get("event_id"),
            {
                "humidity": board.get_avg_humidity(),
            }
        )
    )

    topics = [
        ping,
        get_led_state,
        set_led_state,
        get_temp,
        get_humidity
    ]

    formated_topics = "\n- ".join(t.full_topic_name.decode("utf-8") for t in topics)
    sub_formated_topics = "\n- ".join(t.response_topic_name.decode("utf-8") for t in topics)

    print(f"topics:\n- {formated_topics}")
    print(f"publish topics:\n- {sub_formated_topics}")

    for topic in topics:
        mqtt_client.subscribe(topic.full_topic_name)

    return {topic.full_topic_name: topic for topic in topics}


def mqtt_callback(topics, client, topic, msg):
    print("Message received...")
    message = ujson.loads(msg)
    print(topic, message)

    topic_infos = topics.get(topic)

    if not topic_infos:
        print(f"Unknown topic: {topic}")
        return

    topic_infos.handler(client, topic_infos, message)

    print("Done mqtt callback")
