import time

import matplotlib.colors
import streamlit as st

from actions import set_led, get_temp, get_humidity, get_led_state, TEMP, HUMIDITY

if 'pending_color_change' not in st.session_state:
    st.session_state['pending_color_change'] = False

st.set_page_config(page_title="Workshop IoT Core")
st.title("Workshop IoT Core")

st.subheader("Config")

thing_name = st.text_input("Thing name", key="thing_name")
aws_endpoint = st.text_input("Endpoint")

st.subheader("Led")

led_on_checkbox = st.checkbox("Led on", value=True, key="led_state")

if not st.session_state.get('pending_color_change', False):
    if not thing_name and not aws_endpoint:
        st.error("Thing name not set")
    else:
        get_led_state(aws_endpoint, thing_name)

led_color_picker = st.color_picker("Led color", key="color")

send_set_led_button = st.button("Set led", type="primary")

if send_set_led_button:
    if not thing_name and not aws_endpoint:
        st.error("Thing name not set")
    else:
        with st.spinner("Running set led..."):
            set_led(led_on_checkbox, matplotlib.colors.to_rgb(led_color_picker),
                    aws_endpoint, thing_name)

st.subheader("Temp and humidity")

if not thing_name and not aws_endpoint:
    st.error("Thing name not set")
else:
    with st.spinner("Running get temp..."):
        get_temp(aws_endpoint, thing_name)

    with st.spinner("Running get humidity..."):
        get_humidity(aws_endpoint, thing_name)

    st.text(f"Temp: {TEMP}")
    st.text(f"Humidity: {HUMIDITY}")

    time.sleep(1)
