import functools
import json
import uuid

import boto3
import matplotlib.colors
import streamlit as st
from awscrt.mqtt import QoS
from awsiot import mqtt_connection_builder

SET_LED_TOPIC = "led/status/write"
GET_LED_TOPIC = "led/status/read"
GET_TEMP_TOPIC = "am2320/temperature/read"
GET_HUMIDITY_TOPIC = "am2320/humidity/read"
PING_TOPIC = "ping"

_IOT_CORE_CLIENT = None
_MQTT_CLIENT = None

HUMIDITY = 0
TEMP = 0

def callback(thing_name: str, topic: str, payload: bytes, dup: bool, qos: QoS,
             retain: bool, **kwargs):
    print(payload)
    match topic:
        case s if s == f"pico/{thing_name}/data/ping":
            st.session_state["ping"] = json.loads(payload.decode("utf-8"))
        case s if s == f"pico/{thing_name}/data/led/status/read":
            st.session_state["color"] = matplotlib.colors.to_hex(json.loads(payload.decode("utf-8"))["color"])
            st.session_state["led_state"] = json.loads(payload.decode("utf-8"))["state"]
        case s if s == f"pico/{thing_name}/data/led/status/write":
            st.info("Set led successful")
        case s if s == f"pico/{thing_name}/data/am2320/temperature/read":
            global TEMP
            TEMP = json.loads(payload.decode("utf-8"))["temp"]
        case s if s == f"pico/{thing_name}/data/am2320/humidity/read":
            global HUMIDITY
            HUMIDITY = json.loads(payload.decode("utf-8"))[
                "humidity"]


def get_clients(endpoint):
    global _IOT_CORE_CLIENT
    global _MQTT_CLIENT

    if not _IOT_CORE_CLIENT:
        _IOT_CORE_CLIENT = boto3.client("iot-data", endpoint_url=endpoint,
                                        region_name="eu-west-1")

    if not _MQTT_CLIENT:
        if not st.session_state.get('thing_name'):
            return

        mqtt_connection = mqtt_connection_builder.mtls_from_path(
            endpoint=endpoint.removeprefix("https://"),
            port=8883,
            cert_filepath="../microcontroller/front-certificate.pem.crt",
            pri_key_filepath="../microcontroller/front-private.pem.key",
            client_id=st.session_state.get('thing_name'),
            clean_session=True,
            keep_alive_secs=30,
        )
        connect_future = mqtt_connection.connect()
        # Future.result() waits until a result is available
        connect_future.result()
        topics = {
            f"pico/{st.session_state.get('thing_name')}/data/ping",
            f"pico/{st.session_state.get('thing_name')}/data/led/status/read",
            f"pico/{st.session_state.get('thing_name')}/data/led/status/write",
            f"pico/{st.session_state.get('thing_name')}/data/am2320/temperature/read",
            f"pico/{st.session_state.get('thing_name')}/data/am2320/humidity/read"
        }

        for topic in topics:
            mqtt_connection.subscribe(topic, QoS.AT_LEAST_ONCE, callback=functools.partial(
                callback, st.session_state["thing_name"]))

    return _IOT_CORE_CLIENT, _MQTT_CLIENT


def ping(endpoint, thing_name):
    event_id = uuid.uuid4().hex

    ping_params = {
        "event_id": event_id
    }

    boto3_client, mqtt_client = get_clients(endpoint)

    prefix = f"pico/{thing_name}/"

    resp = boto3_client.publish(
        topic=prefix + PING_TOPIC,
        qos=0,
        retain=False,
        payload=json.dumps(ping_params).encode("utf-8"),
        payloadFormatIndicator='UTF8_DATA',
        contentType='application/json',
        responseTopic=prefix + PING_TOPIC + "/data",
        messageExpiry=30
    )

    print(resp)


def set_led(led_on: bool, led_color: tuple[float, float, float], endpoint, thing_name):
    event_id = uuid.uuid4().hex

    r, g, b = led_color

    r = int(r * 255)
    g = int(g * 255)
    b = int(b * 255)

    set_params = {
        "color": [r, g, b],
        "state": "on" if led_on else "off",
        "event_id": event_id
    }

    boto3_client, mqtt_client = get_clients(endpoint)

    prefix = f"pico/{thing_name}/"

    resp = boto3_client.publish(
        topic=prefix + SET_LED_TOPIC,
        qos=0,
        retain=False,
        payload=json.dumps(set_params).encode("utf-8"),
        payloadFormatIndicator='UTF8_DATA',
        contentType='application/json',
        responseTopic=prefix + SET_LED_TOPIC + "/data",
        messageExpiry=30
    )

    print(resp)


def get_led_state(endpoint, thing_name):
    event_id = uuid.uuid4().hex

    get_params = {
        "event_id": event_id
    }

    boto3_client, mqtt_client = get_clients(endpoint)

    prefix = f"pico/{thing_name}/"

    resp = boto3_client.publish(
        topic=prefix + GET_LED_TOPIC,
        qos=0,
        retain=False,
        payload=json.dumps(get_params).encode("utf-8"),
        payloadFormatIndicator='UTF8_DATA',
        contentType='application/json',
        responseTopic=prefix + GET_LED_TOPIC + "/data",
        messageExpiry=30
    )

    print(resp)

    return matplotlib.colors.to_hex((0, 0, 0)), None


def get_temp(endpoint, thing_name):
    event_id = uuid.uuid4().hex

    get_params = {
        "event_id": event_id
    }

    boto3_client, mqtt_client = get_clients(endpoint)

    prefix = f"pico/{thing_name}/"

    resp = boto3_client.publish(
        topic=prefix + GET_TEMP_TOPIC,
        qos=0,
        retain=False,
        payload=json.dumps(get_params).encode("utf-8"),
        payloadFormatIndicator='UTF8_DATA',
        contentType='application/json',
        responseTopic=prefix + GET_TEMP_TOPIC + "/data",
        messageExpiry=30
    )

    print(resp)


def get_humidity(endpoint, thing_name):
    event_id = uuid.uuid4().hex

    get_params = {
        "event_id": event_id
    }

    boto3_client, mqtt_client = get_clients(endpoint)

    prefix = f"pico/{thing_name}/"

    resp = boto3_client.publish(
        topic=prefix + GET_HUMIDITY_TOPIC,
        qos=0,
        retain=False,
        payload=json.dumps(get_params).encode("utf-8"),
        payloadFormatIndicator='UTF8_DATA',
        contentType='application/json',
        responseTopic=prefix + GET_HUMIDITY_TOPIC + "/data",
        messageExpiry=30
    )

    print(resp)
